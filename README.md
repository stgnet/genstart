# genstart

Cummins Onan Generator Start/Stop

## Circuit

Uses "NOYITO 2 Channel Relay Module Self-locking Triggering
Interlocking Optocoupler Isolation High Low Level Trigger
for PLC Industrial Control (12V)"

Existing 8 pin DIP microcontroller is replaced with ATTINY85

Pin | Port | Usage
--- | ---- | ---
 1  | PB5  | input 2 (RST)
 2  | PB3  | 5v?
 3  | PB4  | input 1
 4  | GND  | Ground
 5  | PB0  | Relay 2
 6  | PB1  | Relay 1
 7  | PB2  | 0v?
 8  | VCC  | 5v supply

