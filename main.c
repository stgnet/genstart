// generator auto start for Cummins ONAN 2800 and similar
// copyright 2020 by scott@griepentrog.com
//
// The generator has two momentary buttons: START and STOP/PRIME
// These are wired to relays 1 and 2
// Relay closure used to call for generator run on input 1
// Generator run signal is on input 2 (future)

#include "arduino.h"

#define RELAY1 1 // start
#define RELAY2 0 // stop
#define INPUT1 4 // call for generator
#define INPUT2 5 // possible future run signal

int no_run; // defeat run signal check if not responding

void stop() {
	// press the stop button for 1s
	digitalWrite(RELAY2, HIGH);
	delay(1000);
	digitalWrite(RELAY2, LOW);
	delay(500);
}

void prime() {
	// hold the stop button for 5s to prime fuel into engine
	digitalWrite(RELAY2, HIGH);
	delay(5000);
	digitalWrite(RELAY2, LOW);
	delay(500);
}

void start() {
	// hold the start button for 10s to make sure it gets going
	digitalWrite(RELAY1, HIGH);
	delay(10000);
	digitalWrite(RELAY1, LOW);
	delay(500);
}

int call() {
	// input is low for call
	return !digitalRead(INPUT1);
}

int running() {
	// not currently used, must change fuses to
	// disable reset and reuse pin for input
	return !digitalRead(INPUT2);
}

void setup() {
	// preset the i/o pins direction and state
	pinMode(INPUT1, INPUT);
	pinMode(INPUT2, INPUT);
	digitalWrite(INPUT1, HIGH);
	digitalWrite(INPUT2, HIGH);
	pinMode(RELAY1, OUTPUT);
	pinMode(RELAY2, OUTPUT);
	digitalWrite(RELAY1, LOW);
	digitalWrite(RELAY2, LOW);

	// make sure generator isn't already running at power up
	stop();
	delay(3000);

	no_run = running();
}

void loop() {
	// main loop, repeates forever
	while (!call()) {
		// wait here until called
		delay(250);
	}
	prime();
	if (!call()) {
		// there was a call but it disappeared?
		return;
	}
	start();
	delay(30000); // will run for 30s minimum
	while (call()) {
		// wait here until no call
		delay(250);
		if (no_run) continue;
		if (!running()) {
			break;
		}
		break;
	}
	stop();
}
