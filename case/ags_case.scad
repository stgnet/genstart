// auto gen start case (ags-1)

$fn=$preview ? 16 : 32;

pcb_x = 40;
pcb_y = 64.5;

// x distance between holes: inside=30.3 outside=36.7-36.8
// y distance between holes: inside=55.4 outside=61.4
// hole size = 3.3

// mount hole dimension and offset position from 4 corners
hole_d = 2.4;

// distance between holes center to center
hole_x_ctc = 33.6;
hole_y_ctc = 58.4;

// offset from edge of pcb
hole_x = (pcb_x - hole_x_ctc)/2;
hole_y = (pcb_y - hole_y_ctc)/2;

// center of hole to edge assumed to be clear of parts on bottom
post_height = 4;
cone_height = 2;

// space around hole on top
hole_clear_d = 5;

// space above and below pcb
pcb_z_below = 3;
pcb_z_thick = 1.75;
pcb_z_above = 15;

wall_thick = 1.5;
wall_rounding = wall_thick/3;
wall_gap = 0.3;
total_z = wall_thick*2 + pcb_z_below + pcb_z_thick + pcb_z_above;

// wire
wire_d = 3;
wire_depth = 0.9 * wire_d;

wire_left_y = [55.5, 40.5, 25.5, 15.0 ];
wire_bot_x = [/*9.5, 14.5, 19.5, 24.5*/];

// tabs
tab_thick = 6;
tab_extend = 10;
tab_d = 12;

// cutout for trigger terminal access
cutoff_size = [10.5+wall_rounding*2, 8+wall_rounding+wall_thick, total_z-wall_thick-pcb_z_below-pcb_z_thick];
cutoff_pos = [13.5-wall_rounding, -wall_thick, total_z-cutoff_size[2]+0.1];
cutoff_depth = 6;
cutoff_y = 4;

labels=[
    /*
    [9.5, 3, "+",4],
    [14.5, 3, "-",4],
    [19.5, 3, "R",4],
    [24.5, 3, "G",4],
    */
    [5, 55.5, "LM", 4],
    [5, 40.5, "LL", 4],
    [5, 25.5, "NF", 4],
    [5, 15.0, "JF", 4],
];
labels_rot=[
    [pcb_x*.7, pcb_y/2, "STG.NET", 5],
    [pcb_x*.5, pcb_y/2, "AGS-1", 5],
];

led_id = 2.0;
led_od = led_id+2*0.4;
led_od_base = led_od + 4*0.4;
led_height = 14;
leds = [
    [35.7, 34.8, 0], // power
    [35.7, 39.2, 0], // LED1 start
    [35.7, 42.9, 0], // LED2 stop
];

module rcube(size, rr) {
    minkowski() {
        translate([rr, rr, rr])
            cube([size[0]-2*rr, size[1]-2*rr, size[2]-2*rr]);
        sphere(r=rr);
    }
}

module rcylinder(h, d, center, rr) {
    minkowski() {
        translate([0, 0, center?0:rr]) cylinder(h=h-2*rr, d=d-2*rr, center=center);
        sphere(r=rr);
    }
}

module tube(id, od, h) {
    difference() {
        cylinder(h=h, d=od);
        cylinder(h=h, d=id);
    }
}

module tubal(id, top, bot, h) {
    difference() {
        cylinder(h=h, d1=bot, d2=top);
        cylinder(h=h, d=id);
    }
}

// hole for bevel head screw
module screw(length) {
    head_d = 9;
    shaft_d = 4.5;
    // assuming 45 bevel head
    head_h = (head_d-shaft_d)/2;
    translate([0, 0, -head_h])
        cylinder(d1=shaft_d, d2=head_d, h=head_h);
    translate([0, 0, -length])
        cylinder(d=shaft_d, h=length);    
}

// tab with screw hole in it extending x+ and down from z
module tab() {
    difference() {
        union() {
            rr=wall_rounding;
            // extend cube back into support to fill in rounding
            translate([-rr*2, -tab_d/2, -tab_thick])
                rcube([tab_extend+rr*3, tab_d, tab_thick], rr);
            translate([tab_extend, 0, -tab_thick])
                rcylinder(h=tab_thick, d=tab_d, rr=rr);
        }
        translate([tab_extend, 0, 0])
            screw(tab_thick);
    }
}
            

module posts() {
    for (x=[hole_x, pcb_x-hole_x]) {
        for (y=[hole_y, pcb_y-hole_y]) {
            translate([x, y, 0]) {
                children();
            }
        }
    }
}

module wires_left(extend) {
    for (y=wire_left_y) {
        translate([-wall_thick, y, wall_thick+(extend?pcb_z_below/2:pcb_z_below)-wire_depth+wire_d/2]) {
            rotate([0, 90, 0]) {
                hull() {
                    cylinder(h=wall_thick+hole_x, d=wire_d);
                    translate([extend?wall_thick+pcb_z_below:-wire_d/2, 0, 0])
                        cylinder(h=wall_thick+hole_x, d=wire_d*1.25);
                }
            }
        }
    }

}
module wires_bot(extend) {
    for (x=wire_bot_x) {
        translate([x, -wall_thick, wall_thick+pcb_z_below-wire_depth+wire_d/2]) {
            rotate([-90, 0, 0]) {
                hull() {
                    cylinder(h=wall_thick+hole_x, d=wire_d);
                    translate([0, extend?wall_thick+pcb_z_below:-wire_d/2, 0])
                        cylinder(h=wall_thick+hole_x, d=wire_d*1.25);
                }
            }
        }
    }
}

module bottom() {
    translate([wall_gap, wall_gap, 0])
        cube([pcb_x-wall_gap*2, pcb_y-wall_gap*2, wall_thick]);
    // edge around pcb
    difference() {
        union() {
            for (x=[wall_gap, pcb_x-hole_x+wall_gap]) {
                translate([x, wall_gap, wall_thick]) {
                    cube([hole_x-wall_gap*2, pcb_y-wall_gap*2, pcb_z_below]);
                }
            }
        }
        wires_left(false);
    }
    difference() {
        union() {
            for (y=[wall_gap, pcb_y-hole_y+wall_gap]) {
                translate([wall_gap, y, wall_thick]) {
                    cube([pcb_x-wall_gap*2, hole_y-wall_gap*2, pcb_z_below]);
                }
            }
        }
        wires_bot(false);
    }
    // posts
    translate([0, 0, wall_thick]) {
        posts() {
            height = pcb_z_below + pcb_z_thick + post_height;
            cylinder(d=hole_d, h=height);
            translate([0, 0, height])
                cylinder(h=cone_height, d1=hole_d, d2=0);
        }
    }
}

module top() {
    difference() {
        union() {
            translate([-wall_thick/2, -wall_thick/2, total_z-wall_thick])
                cube([pcb_x+wall_thick, pcb_y+wall_thick, wall_thick]);
        }
        
        text_depth=0.8;
        mink_depth=text_depth*5;

        // etch the text labels into the top cover
        for (label=labels) {
            translate([label[0], label[1], wall_thick+pcb_z_below+pcb_z_thick+pcb_z_above+wall_thick-text_depth]) {
                 minkowski() {
                    linear_extrude(text_depth)
                        offset(r=-0.2) text(label[2], size=label[3], valign="center", 
                                halign="center", spacing=1.2, $fn=4);
                    cylinder(mink_depth, 0, mink_depth, $fn=4);
                }
            }
        }
        for (label=labels_rot) {
            translate([label[0], label[1], wall_thick+pcb_z_below+pcb_z_thick+pcb_z_above+wall_thick-text_depth]) rotate([0, 0, -90]) {
                 minkowski() {
                    linear_extrude(text_depth)
                        offset(r=-0.2) text(label[2], size=label[3], valign="center", 
                                halign="center", spacing=1.2, $fn=4);
                    cylinder(mink_depth, 0, mink_depth, $fn=4);
                }
            }
        }
        
        // make holes for led view
        for (led=leds) {
            translate(led)
                translate([0, 0, total_z-wall_thick])
                    cylinder(d=led_id, h=wall_thick);
        }
        // cutout for screw terminals
        translate(cutoff_pos) cube(cutoff_size);
    }

    // led tubes
    for (led=leds) {
        translate(led)
            translate([0, 0, total_z-led_height])
                tubal(id=led_id, bot=led_od, top=led_od_base, h=led_height);
    }

    // sides
    difference() {
        union() {
            for (x=[-wall_thick, pcb_x]) {
                translate([x, -wall_thick, 0])
                    rcube([wall_thick, pcb_y+2*wall_thick, total_z], wall_rounding);
            }
        }
        wires_left(true);
    } 
    difference() {
        union() {
            for (y=[-wall_thick, pcb_y]) {
                translate([-wall_thick, y, 0])
                    rcube([pcb_x+2*wall_thick, wall_thick, total_z], wall_rounding);
            }
        }
        wires_bot(true);
        // cutout for screw terminals
        translate(cutoff_pos) cube(cutoff_size);
    }
    // walls around cutout
    translate([cutoff_pos[0]-wall_rounding, cutoff_pos[1], total_z-cutoff_depth])
        rcube([wall_thick, cutoff_size[1]+wall_rounding, cutoff_depth], wall_rounding);
    translate([cutoff_pos[0]-wall_rounding, cutoff_pos[1], total_z-cutoff_size[2]])
        rcube([wall_thick, cutoff_y, cutoff_size[2]-pcb_z_thick], wall_rounding);
    translate([cutoff_pos[0]+cutoff_size[0]-wall_rounding, cutoff_pos[1], total_z-cutoff_depth])
        rcube([wall_thick, cutoff_size[1]+wall_rounding, cutoff_depth], wall_rounding);
    translate([cutoff_pos[0]+cutoff_size[0]-wall_rounding, cutoff_pos[1], total_z-cutoff_size[2]])
        rcube([wall_thick, cutoff_y, cutoff_size[2]-pcb_z_thick], wall_rounding);
    translate([cutoff_pos[0]-wall_rounding, cutoff_pos[1]+cutoff_size[1]-wall_rounding, total_z-cutoff_depth])
        rcube([cutoff_size[0]+wall_thick, wall_thick, cutoff_depth], wall_rounding);
    
    // posts
    difference() {
        union() {
            translate([0, 0, wall_thick+pcb_z_below+pcb_z_thick]) posts() {
                cylinder(d1=hole_clear_d, d2=max(hole_x*2, hole_y*2)+wall_thick,
                            h=total_z-wall_thick-pcb_z_below-pcb_z_thick);
            }
        }
        translate([0, 0, wall_thick]) {
            posts() {
                height = pcb_z_below + post_height;
                cylinder(d=hole_d+0.05, h=height);
                translate([0, 0, height])
                    cylinder(h=cone_height, d1=hole_d, d2=0);
            }
        }
    }
    // tabs
    translate([pcb_x+wall_thick, pcb_y/2, total_z])
        tab();
    translate([pcb_x/2, pcb_y+wall_thick, total_z])
        rotate([0, 0, 90])
            tab();
}

// show pcb
/*translate([0, 0, wall_thick+pcb_z_below])
    #cube([pcb_x, pcb_y, pcb_z_thick]);
*/
translate([0, 0, total_z]) rotate([0, 180, 0]) top();

translate([2, 0, 0]) bottom();

for (y=[5, pcb_y-5])
    translate([0, y, 0]) 
        cube([5, 0.4*4, 0.5]);

/*
translate([100, 0, 20]) rotate([0, 180, 0])
scale([0.1, 0.098, 0.01])
  surface(file = "relay-board.png");
*/
